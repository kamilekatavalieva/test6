<?php

namespace App\Factory;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Product>
 *
 * @method static Product|Proxy createOne(array $attributes = [])
 * @method static Product[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Product|Proxy find(object|array|mixed $criteria)
 * @method static Product|Proxy findOrCreate(array $attributes)
 * @method static Product|Proxy first(string $sortedField = 'id')
 * @method static Product|Proxy last(string $sortedField = 'id')
 * @method static Product|Proxy random(array $attributes = [])
 * @method static Product|Proxy randomOrCreate(array $attributes = [])
 * @method static Product[]|Proxy[] all()
 * @method static Product[]|Proxy[] findBy(array $attributes)
 * @method static Product[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Product[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ProductRepository|RepositoryProxy repository()
 * @method Product|Proxy create(array|callable $attributes = [])
 */
final class ProductFactory extends ModelFactory
{
    private ParameterBagInterface $params;

    public function __construct(ParameterBagInterface $params)
    {
        parent::__construct();
        $this->params = $params;

    }

    /**
     * @return array
     */
    protected function getDefaults(): array
    {
        return [
            'denomination' => self::faker()->word(),
            'description' => self::faker()->text(),
            'price' => self::faker()->randomFloat(2,100,1000),
            'image' => $this->getImage(rand(1, 3))
        ];
    }

    /**
     * @return $this
     */
    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Product $product): void {})
        ;
    }

    /**
     * @return string
     */
    protected static function getClass(): string
    {
        return Product::class;
    }

    /**
     * @param int $image_number
     * @return string
     */
    private function getImage(int $image_number): string
    {
        $path = $this->params->get('kernel.project_dir') . '/src/Resources/images/' . '/' . $image_number . '.jpg';
        $image_name = md5($path) . '.jpg';
        $image = file_get_contents($path);
        $new_path = $this->params->get('kernel.project_dir') . '/public/images/' . '/' . $image_name;
        file_put_contents($new_path, $image);
        return $image_name;
    }
}
