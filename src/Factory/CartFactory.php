<?php

namespace App\Factory;

use App\Entity\Cart;
use App\Entity\User;
use App\Repository\CartRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;


/**
 * @extends ModelFactory<Cart>
 *
 * @method static Cart|Proxy createOne(array $attributes = [])
 * @method static Cart[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Cart|Proxy find(object|array|mixed $criteria)
 * @method static Cart|Proxy findOrCreate(array $attributes)
 * @method static Cart|Proxy first(string $sortedField = 'id')
 * @method static Cart|Proxy last(string $sortedField = 'id')
 * @method static Cart|Proxy random(array $attributes = [])
 * @method static Cart|Proxy randomOrCreate(array $attributes = [])
 * @method static Cart[]|Proxy[] all()
 * @method static Cart[]|Proxy[] findBy(array $attributes)
 * @method static Cart[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Cart[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static CartRepository|RepositoryProxy repository()
 * @method Cart|Proxy create(array|callable $attributes = [])
 */
final class CartFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return array
     */
    protected function getDefaults(): array
    {
        return [
            'denomination' => self::faker()->sentence(),
            'date' => self::faker()->dateTime(),
            'number' => self::faker()->numberBetween(1, 100),
        ];
    }

    /**
     * @return $this
     */
    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Cart $cart): void {})
        ;
    }

    /**
     * @return string
     */
    protected static function getClass(): string
    {
        return Cart::class;
    }
}
