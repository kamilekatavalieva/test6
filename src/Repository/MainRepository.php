<?php

namespace App\Repository;

use App\Entity\Main;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Main|null find($id, $lockMode = null, $lockVersion = null)
 * @method Main|null findOneBy(array $criteria, array $orderBy = null)
 * @method Main[]    findAll()
 * @method Main[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Main::class);
    }


    /**
     * @param Main $entity
     * @param bool $flush
     */
    public function add(Main $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }


    /**
     * @param Main $entity
     * @param bool $flush
     */
    public function remove(Main $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
