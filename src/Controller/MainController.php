<?php

namespace App\Controller;

use App\Entity\Main;
use App\Form\MainType;
use App\Repository\MainRepository;
use App\Services\MessageGenerator;
use App\Services\SiteUpdateManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/main')]
class MainController extends AbstractController
{

    #[Route('/', name: 'app_main_index', methods: ['GET'])]
    public function index(MainRepository $mainRepository, LoggerInterface $logger, MessageGenerator $messageGenerator): Response
    {

        $message = $messageGenerator->getHappyMessage();
        $this->addFlash('success', $message);

        return $this->render('main/index.html.twig', [
//            'mains' => $mainRepository->findAll(),
        'mains' => $message
        ]);

    }

    #[Route('/new', name: 'app_main_new', methods: ['GET', 'POST'])]
    public function new(Request $request, MainRepository $mainRepository): Response
    {
        $main = new Main();
        $form = $this->createForm(MainType::class, $main);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mainRepository->add($main);
            return $this->redirectToRoute('app_main_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('main/new.html.twig', [
            'main' => $main,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_main_show', methods: ['GET'])]
    public function show(Main $main): Response
    {
        return $this->render('main/show.html.twig', [
            'main' => $main,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_main_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Main $main, MainRepository $mainRepository): Response
    {
        $form = $this->createForm(MainType::class, $main);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mainRepository->add($main);
            return $this->redirectToRoute('app_main_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('main/edit.html.twig', [
            'main' => $main,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_main_delete', methods: ['POST'])]
    public function delete(Request $request, Main $main, MainRepository $mainRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$main->getId(), $request->request->get('_token'))) {
            $mainRepository->remove($main);
        }

        return $this->redirectToRoute('app_main_index', [], Response::HTTP_SEE_OTHER);
    }
}
