<?php

namespace App\Entity;

use App\Repository\MainRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MainRepository::class)]
class Main
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $denomination;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getDenomination(): ?string
    {
        return $this->denomination;
    }

    /**
     * @param string $denomination
     * @return $this
     */
    public function setDenomination(string $denomination): self
    {
        $this->denomination = $denomination;

        return $this;
    }
}
