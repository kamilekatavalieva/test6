<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends Fixture
{
    private \Faker\Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }
    public function load(ObjectManager $manager): void
    {
//        $categories = $manager->getRepository(Category::class)->findAll();
//        for ($i = 0; $i < 50; $i++) {
//
//            $product = new Product();
//            $product->setDenomination($this->faker->word(2));
//            $product->setDescription($this->faker->paragraph(2));
//            $product->setCategory($categories[array_rand($categories)]);
//            $product->setPrice($this->faker->biasedNumberBetween(200,1000));
//            $product->setImage($this->faker->image());
//            $manager->persist($product);
//
//        }
//        $manager->flush();
    }
}
