<?php

namespace App\DataFixtures;


use App\Entity\Category;
use App\Entity\User;
use App\Factory\CartFactory;
use App\Factory\CategoryFactory;
use App\Factory\ProductFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {

        $user = new User();
        $user->setEmail('admin@admin.com');
        $user->setPassword(password_hash('password', PASSWORD_DEFAULT));
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);
        $manager->flush();

        CategoryFactory::createMany(5);

        UserFactory::createMany(10, function () {
            return [
                'carts' => CartFactory::createMany(rand(0, 10), function () {
                    return [
                        'products' => ProductFactory::createMany(rand(1, 10), function () {
                            return [
                                'category' => CategoryFactory::random(),
                            ];
                        })
                    ];
                })
            ];
        });
        $manager->flush();
    }

}
